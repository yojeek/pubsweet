# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.1.7](https://coko.gitlab.foundation/pubsweet/pubsweet/compare/create-pubsweet-app@0.1.6...create-pubsweet-app@0.1.7) (2020-03-16)

**Note:** Version bump only for package create-pubsweet-app





## [0.1.6](https://coko.gitlab.foundation/pubsweet/pubsweet/compare/create-pubsweet-app@0.1.5...create-pubsweet-app@0.1.6) (2020-03-04)

**Note:** Version bump only for package create-pubsweet-app





## [0.1.5](https://coko.gitlab.foundation/pubsweet/pubsweet/compare/create-pubsweet-app@0.1.4...create-pubsweet-app@0.1.5) (2020-02-28)

**Note:** Version bump only for package create-pubsweet-app





## [0.1.4](https://coko.gitlab.foundation/pubsweet/pubsweet/compare/create-pubsweet-app@0.1.3...create-pubsweet-app@0.1.4) (2020-02-26)

**Note:** Version bump only for package create-pubsweet-app





## [0.1.3](https://coko.gitlab.foundation/pubsweet/pubsweet/compare/create-pubsweet-app@0.1.2...create-pubsweet-app@0.1.3) (2020-01-29)

**Note:** Version bump only for package create-pubsweet-app





## [0.1.2](https://coko.gitlab.foundation/pubsweet/pubsweet/compare/create-pubsweet-app@0.1.1...create-pubsweet-app@0.1.2) (2020-01-23)

**Note:** Version bump only for package create-pubsweet-app





## [0.1.1](https://coko.gitlab.foundation/pubsweet/pubsweet/compare/create-pubsweet-app@0.1.0...create-pubsweet-app@0.1.1) (2019-12-11)

**Note:** Version bump only for package create-pubsweet-app





# 0.1.0 (2019-11-11)


### Features

* add placeholder package for create-pubsweet-app ([c2c35cf](https://coko.gitlab.foundation/pubsweet/pubsweet/commit/c2c35cfaa7db9f0855624713ff0b56f3d79c4eb0))
