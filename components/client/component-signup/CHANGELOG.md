# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.18](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.17...pubsweet-component-signup@2.1.18) (2020-03-16)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.17](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.16...pubsweet-component-signup@2.1.17) (2020-03-04)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.16](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.15...pubsweet-component-signup@2.1.16) (2020-02-28)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.15](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.14...pubsweet-component-signup@2.1.15) (2020-02-26)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.14](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.13...pubsweet-component-signup@2.1.14) (2020-01-29)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.13](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.12...pubsweet-component-signup@2.1.13) (2020-01-23)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.11...pubsweet-component-signup@2.1.12) (2019-12-11)


### Bug Fixes

* small styling fixes ([3c6a11f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3c6a11f0c558a9b6a433c9b122b8226133461bf0))





## [2.1.11](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.10...pubsweet-component-signup@2.1.11) (2019-11-11)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.9...pubsweet-component-signup@2.1.10) (2019-09-11)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.9](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.8...pubsweet-component-signup@2.1.9) (2019-09-04)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.8](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.7...pubsweet-component-signup@2.1.8) (2019-08-30)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.7](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.6...pubsweet-component-signup@2.1.7) (2019-08-08)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.5...pubsweet-component-signup@2.1.6) (2019-08-05)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.4...pubsweet-component-signup@2.1.5) (2019-07-12)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.3...pubsweet-component-signup@2.1.4) (2019-07-09)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.2...pubsweet-component-signup@2.1.3) (2019-07-03)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.1...pubsweet-component-signup@2.1.2) (2019-06-28)

**Note:** Version bump only for package pubsweet-component-signup





## [2.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.1.0...pubsweet-component-signup@2.1.1) (2019-06-24)

**Note:** Version bump only for package pubsweet-component-signup





# [2.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.0.5...pubsweet-component-signup@2.1.0) (2019-06-21)


### Features

* **signup:** improve error-handling in client component ([287f917](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/287f917)), closes [#447](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/447)





## [2.0.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.0.4...pubsweet-component-signup@2.0.5) (2019-06-13)

**Note:** Version bump only for package pubsweet-component-signup





## [2.0.4](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.0.3...pubsweet-component-signup@2.0.4) (2019-06-12)

**Note:** Version bump only for package pubsweet-component-signup





## [2.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.0.2...pubsweet-component-signup@2.0.3) (2019-05-27)

**Note:** Version bump only for package pubsweet-component-signup





## [2.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.0.1...pubsweet-component-signup@2.0.2) (2019-04-25)

**Note:** Version bump only for package pubsweet-component-signup





## [2.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@2.0.0...pubsweet-component-signup@2.0.1) (2019-04-18)

**Note:** Version bump only for package pubsweet-component-signup





# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-signup@1.0.41...pubsweet-component-signup@2.0.0) (2019-04-09)


### Bug Fixes

* **signup:** use older version of recompose ([2769675](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2769675))


### Code Refactoring

* temporarily remove unmigrated components ([32db6ad](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/32db6ad))


### Features

* **signup:** add logo to signup form ([cd63d9d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cd63d9d))
* **signup:** formik enable for signup ([96dc09f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/96dc09f))


### BREAKING CHANGES

* A lot of unmigrated (not yet moved from REST/Redux to GraphQL/Apollo system) bits
have changed. There might be some breaking changes as a result. This is a big migration involving
big changes - if you encounter anything weird, please contact us on GitLab or on Mattermost.
